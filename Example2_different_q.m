%Example2: 
%Closed-Form for variable f_{d}. Compare with different wegihts of f__{d}.


x0 = log10(5); 
r = 7;
b = [0.2 0.3 0.4]'; 
bhat = (-1/sqrt(r))*b; 
c = -[0.1 0.5 1]' ; 
tf = 24;
d = -2; 
e = d;
eta = 1; 
q1 = -1;
q2 = -2; 

%Discrtize the the problem with time step dt:
dt =0.1;

%Time vector:
t = 0:dt:tf;
h = length(t);
%Write the linear system matricies (discrete time):
A = 1; B = dt*bhat';  

%Closed form solution: q=-1
uct1 = exp(bhat*(-d*(t-tf)+e)+c);
nuct1 = sum(uct1);
vc1 = (-1/(2*q1*eta))*lambertw(0, -2*q1*eta*exp(-1)*nuct1);
uc1 = (uct1./(nuct1)).*vc1;
lamc1 = -d*(t-tf)+e;

xc1 = zeros(1,length(t)); 
xc1(1) = x0; 

%Closed form solution: q=-2
uct2 = exp(bhat*(-d*(t-tf)+e)+c);
nuct2 = sum(uct2);
vc2 = (-1/(2*q2*eta))*lambertw(0, -2*q2*eta*exp(-1)*nuct2);
uc2 = (uct2./(nuct2)).*vc2;
lamc2 = -d*(t-tf)+e;

xc2 = zeros(1,length(t)); 
xc2(1) = x0; 

%Closed form solution: fixed f_d=1
ucf = exp(bhat*(-d*(t-tf)+e)+c); 
ucf = ucf./(sum(ucf));
lamcf = -d*(t-tf)+e;
xcf = zeros(1,length(t)); 
xcf(1) = x0; 

for i=1:(length(t)-1)
xcf(i+1) = A*xcf(i)+B*ucf(:,i);
xc1(i+1) = A*xc1(i)+B*uc1(:,i);
xc2(i+1) = A*xc2(i)+B*uc2(:,i);
end


figure 
tv = tiledlayout(3,1);

nexttile
p1 = plot(t,10.^(xc1),'--','LineWidth',1.5,'Color','k');
hold on 
p2 = plot(t,10.^(xc2),':','LineWidth',1.5,'Color','k');

p3 = plot(t,10.^(xcf),'Color','k','LineWidth',0.6);
axis([-inf 24 -inf inf])

leg=legend('Varying rate, q=-1','Varying rate, q=-2','Fixed rate');
leg.Interpreter = 'latex';
p1.Color =  [0.6350 0.0780 0.1840]; 
p2.Color = [0.6350 0.0780 0.1840];
p3.Color = [0.6350 0.0780 0.1840];
grid on 
ylabel('$\beta(t)$','Interpreter','latex')
nexttile 
plot(t,uc1(1,:)./vc1,'--','LineWidth',1.5)
hold on
plot(t,uc1(2,:)./vc1,'--','LineWidth',1.5)
plot(t,uc1(3,:)./vc1,'--','LineWidth',1.5)

plot(t,uc2(1,:)./vc2,':','Color',[0 0.4470 0.7410],'LineWidth',1.6);
plot(t,uc2(2,:)./vc2,':','Color',[0.8500 0.3250 0.0980],'LineWidth',1.6);
plot(t,uc2(3,:)./vc2,':','Color',[0.9290 0.6940 0.1250],'LineWidth',1.6);

hold on 
pp1 = plot(t,ucf(1,:),'Color',[0 0.4470 0.7410],'LineWidth',0.6);
pp2 = plot(t,ucf(2,:),'Color',[0.8500 0.3250 0.0980],'LineWidth',0.6);
pp3 = plot(t,ucf(3,:),'Color',[0.9290 0.6940 0.1250],'LineWidth',0.6);
axis([-inf 24 -inf inf])


leg = legend([pp1 pp2 pp3],'$\bar{u}_{1}$','$\bar{u}_{2}$','$\bar{u}_{3}$');
leg.Interpreter = 'latex';
grid on 
ylabel('$\bar{\mbox{\textit{\textbf{u}}}}(t)$','Interpreter','latex')

nexttile
plot(t,vc1,'--','LineWidth',1.5,'Color',[0.6350 0.0780 0.1840])
hold on 
plot(t,vc2,':','LineWidth',1.5,'Color',[0.6350 0.0780 0.1840])
hold on 
plot(t,ones(size(t)),'LineWidth',1.5,'Color',[0.6350 0.0780 0.1840])

grid on

ylabel('$f_{d}(t)$','Interpreter','latex')
axis([-inf 24 -inf inf])