This repository contains the code for the paper: **Entropy for Optimal Control on a Simplex with an Application to
Behavioural Nudging (Accepted to L-CSS with CDC option).**


## Abstract
We study the utilization of the entropy function of inputs in solving an Optimal Control Problem (OCP) with linear dynamics and inputs constrained to a variable-sized simplex in which the size is also an input. By using the entropy function as part of the objective functional in the OCP, we are able to derive a closed-form solution. Additionally, we present an example of how the studied OCP can be applied to choose between nudging techniques to discourage a specific behavior, such as non-adherence to medication, through the lens of behavioral momentum theory.

## Behavioral Momentum Model
The behavioral momentum model used to generate the results is as following
$$\dot{x}(t)= f_{d}(t)\frac{-1}{\sqrt{r(t)}}\boldsymbol{b}^{\mathrm{T}}(t)\bar{\boldsymbol{u}}(t),$$
where $f_{d}(t)$ is the average distrubtors rate, $r(t)$ is the average reinforcer rate, $\boldsymbol{b}$ is the effect of each type of distrubtors, and $\bar{\boldsymbol{u}}(t)$ is a vector of the probabilities of each type of distrubtors. The value $x(t)=\log_{10}(\beta(t))$ where $\beta(t)$ is the average rate of a behavior. 

## Optimal Control Problem
To define the OCPs of interest in this work, we first define

$$L\left(\boldsymbol{x},\boldsymbol{u},t\right):=\frac{1}{\eta}\phi(\boldsymbol{u})\! + \!\boldsymbol{c}^{\mathrm{T}}\!(t)\boldsymbol{u}\!+\! \boldsymbol{d}^{\mathrm{T}}\!\boldsymbol{x},~S\left(\boldsymbol{x}\right) := \boldsymbol{e}^{\mathrm{T}}\boldsymbol{x},$$

with $`\eta>0`$, $\phi(\boldsymbol{u})$ being the entropy, $\boldsymbol{c}(t)\in\mathbb{R}^{n_{u}}$ being continuously differentiable, $\boldsymbol{d}\in\mathbb{R}^{n_{x}}$, and $\boldsymbol{e}\in\mathbb{R}^{n_{x}}$. The first OCP of interest (OCP1) has the following form

$$\underset{\boldsymbol{u}}{\text{max}} ~   \int_{t_{0}}^{t_f} L\left(\boldsymbol{x}(t),\boldsymbol{u}(t),t\right)\!dt+S\left(\boldsymbol{x}(t_{f})\right)$$

subject to

$$\dot{\boldsymbol{x}}(t) = \boldsymbol{A}\boldsymbol{x}(t) + \boldsymbol{B}(t)\boldsymbol{u}(t),~\boldsymbol{x}(t_{0}) = \boldsymbol{x}_0,$$


$$ \boldsymbol{u}(t)\in\triangle_{n_{u}}^{1} $$


where $`\boldsymbol{x}_{0}\in\mathbb{R}^{n_{x}}`$, $\boldsymbol{A}\in\mathbb{R}^{n_{x}\times n_{x}}$, and $\boldsymbol{B}(t)\in\mathbb{R}^{n_{x}\times n_{u}}$ is continuously differentiable.
Additionally, we consider a second OCP (OCP2) in which $v$ is also an input. For OCP2, the objective is modified by letting $\tilde{L}\left(\boldsymbol{x},\boldsymbol{u},v,t\right)=L\left(\boldsymbol{x},\boldsymbol{u},t\right)+qv^2$ with $`q<0`$ and writing

$$\underset{\boldsymbol{u},v}{\text{max}} ~   \int_{t_{0}}^{t_f} \tilde{L}\left(\boldsymbol{x}(t),\boldsymbol{u}(t),v(t),t\right)\!dt+S\left(\boldsymbol{x}(t_{f})\right)$$

subject to 

$$\dot{\boldsymbol{x}}(t) = \boldsymbol{A}\boldsymbol{x}(t) + \boldsymbol{B}(t)\boldsymbol{u}(t),~\boldsymbol{x}(t_{0}) = \boldsymbol{x}_0,$$


$$v(t)-\boldsymbol{1}^{\mathrm{T}}\boldsymbol{u}(t)=0,~\boldsymbol{u}(t)\geq_{e}0,~v(t)\geq 0.$$






**NOTE:** Remember to download CVX from http://cvxr.com/cvx/download/ to run the examples with CVX. 
