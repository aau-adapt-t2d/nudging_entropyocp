%Example3: Recieding Horizon case

clear all 
close all 
rng(0)
sigmoid = @(x) 1./(1+exp(-x));
%Receding Horizon Case: 
x0 = log10(5); 
b = [0.2 0.3 0.4]'; 
c = -[0.1 0.5 1]' ; 
tf = 24;
d = -2; 
e = d;
eta = 1;
etap = 10*eta;
q1 = -2;
qp = 0.5*q1; 
qbar = (q1+qp);
rho = 5;
%Discrtize the the problem with time step dt:
dt =1/7;

%Time vector:
t = 0:dt:tf;
r = 7*ones(1,length(t));
h = length(t);
%Write the linear system matricies (discrete time):
bhat = (-1./sqrt(r)).*b.*[0.5*(1+exp(-0.2*t));0.5*(1+exp(-0.2*t));ones(size(t))]+(-1./sqrt(r)).*[zeros(size(t));zeros(size(t));-0.4*sigmoid(5*(t-5))+0.4*sigmoid(5*(t-15))]; 
A = 1; B = dt*bhat'; 
uct = exp(bhat.*(-d*(t-tf)+e)+c);
nuct = sum(uct);
vc = (-1/(2*q1*eta))*lambertw(0, -2*q1*eta*exp(-1)*nuct);
uc = (uct./(nuct)).*vc;
lamc = -d*(t-tf)+e;
xc = zeros(1,length(t)); 
xc(1) = x0; 

%For RH: 
BRH = B(1,:); %Initial B matrix
xrhe = x0+0.1*rand; %initial estimate of x0
 
urh = uc; %initial recieding horizon (tf=1)
count = 1; 
vrh = vc;
xrh = zeros(1,length(t)); 
xrh(1) = x0; %Real state for the rh case.
feed = 7;
tfr = tf;
urha = [];
vrha = [];
for i=1:(length(t)-1)
xc(i+1) = A*xc(i)+B(i,:)*uc(:,i);
xrh(i+1) = A*xrh(i)+B(i,:)*urh(:,count);
urha = [urha,urh(:,count)];
vrha = [vrha,vrh(:,count)];
count = count +1;
if(and(not(mod(t(i),feed*dt)),i>1))
    %Compute new u
    tnew = t(i):dt:(tfr);
    vp = vrh(:,count-1);
    up = urh(:,count-1);
    etabar = 1./(1/eta+1/etap*exp(-rho*(tnew-t(i))));
    bhatr = B(i-2,:)'/dt+0.25*norm(B(i-2,:)')*randn(size(B(i-2,:)'));
    for j=1:length(tnew)
    urh(:,j) = exp(etabar(j)*bhatr.*(-d*(tnew(j)-tfr)+e)+etabar(j)*c+etabar(j)*1/etap*exp(-rho*tnew(j)).*log(up));
    end
    nuct = sum(urh);
    for j=1:length(tnew)
    qpt = qp*exp(-rho*(tnew(j)-t(i)));
    qbar = q1+qpt;
    vrh(j) = (-1/(2*qbar*etabar(j))).*lambertw(0, -2*qbar*etabar(j)*exp(-1)*nuct(j)*exp(-2*etabar(j)*qpt*vp));
    end
    urh = (urh./(nuct)).*vrh;
    count = 1;

end
end

figure 
tv = tiledlayout(3,1);
%Plotting:
%figure
nexttile
p1 = plot(t,10.^(xc),'LineWidth',0.6,'Color','k');
hold on 
p2 = plot(t,10.^(xrh),'--','LineWidth',1.5,'Color','k');

axis([-inf 24 -inf inf])

leg=legend('Nominal','Receding Horizon');
leg.Interpreter = 'latex';
p1.Color =  [0.6350 0.0780 0.1840]; 
p2.Color = [0.6350 0.0780 0.1840];
grid on 
ylabel('$\beta(t)$','Interpreter','latex')
nexttile 
pp1 = plot(t,uc(1,:)./vc,'LineWidth',0.6);
hold on
pp2 = plot(t,uc(2,:)./vc,'LineWidth',0.6);
pp3 = plot(t,uc(3,:)./vc,'LineWidth',0.6);
hold on
plot(t(1:end-1),urha(1,:)./vrha,'--','Color',[0 0.4470 0.7410],'LineWidth',1.5);
plot(t(1:end-1),urha(2,:)./vrha,'--','Color',[0.8500 0.3250 0.0980],'LineWidth',1.5);
plot(t(1:end-1),urha(3,:)./vrha,'--','Color',[0.9290 0.6940 0.1250],'LineWidth',1.5);
axis([-inf 24 0 inf])


axis([-inf 24 -inf inf])


leg = legend([pp1 pp2 pp3],'$\bar{u}_{1}$','$\bar{u}_{2}$','$\bar{u}_{3}$');
leg.Interpreter = 'latex';
grid on 
ylabel('$\bar{\mbox{\textit{\textbf{u}}}}(t)$','Interpreter','latex')

nexttile
plot(t,vc,'LineWidth',0.6,'Color',[0.6350 0.0780 0.1840])
hold on 
plot(t(1:end-1),vrha,'--','LineWidth',1.5,'Color',[0.6350 0.0780 0.1840])

grid on
xlabel('Time [Week]','Interpreter','latex')
ylabel('$f_{d}(t)$','Interpreter','latex')
axis([-inf 24 -inf inf])