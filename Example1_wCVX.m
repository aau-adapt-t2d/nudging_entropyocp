%Example1:
%Compare Closed-form with CVX. Fixed f_{d}.


close all 
clear all 
x0 = log10(5); 
r = 7;
b = [0.2 0.3 0.4]'; 
bhat = (-1/sqrt(r))*b; 
c = -[0.1 0.5 1]' ; 
tf = 24;
d = -2; 
e = d;
eta = 1; 

%Discrtize the the problem with time step dt:
dt =0.1;

%Time vector:
t = 0:dt:tf;
h = length(t);
%Write the linear system matricies (discrete time):
A = 1; B = dt*bhat'; 
%Closed form solution:
uc = exp(bhat*(-d*(t-tf)+e)+c); 
uc = uc./(sum(uc));
lamc = -d*(t-tf)+e;
xc = zeros(1,length(t)); 
xc(1) = x0; 

for i=1:(length(t)-1)
xc(i+1) = A*xc(i)+B*uc(:,i);
end

% CVX
cvx_begin
    variable u(3,h-1)
    variable x(h)
    dual variable lam{h-1}
    cost = 0;
    for i=1:h-1
    cost = cost + (1/eta)*sum(entr(u(:,i)))+d*x(i)+c'*u(:,i);
    end
    maximize(dt*(cost)+e*x(end))
    subject to
    x(1) == x0; %initial condition
    %x(end) == xf;
    for i=1:h-1
        lam{i} : x(i+1) == A*x(i) + B*u(:,i);
    end
    
    for i=1:h-1
    sum(u(:,i)) == 1;
    u(:,i)>=0;
    end
cvx_end

figure 
tv = tiledlayout(3,1,"TileSpacing","compact");
%Plotting:
%figure
nexttile
p1 = plot(t,10.^(x),'--','LineWidth',1.5,'Color','k');
hold on 
p2 = plot(t,10.^(xc),'Color','k','LineWidth',0.6);
axis([-inf tf -inf inf])

leg=legend('Numerical','Closed-Form');
leg.Interpreter = 'latex';
p1.Color =  [0.6350 0.0780 0.1840]; 
p2.Color = [0.6350 0.0780 0.1840];
grid on 
ylabel('$\beta(t)$','Interpreter','latex')
nexttile 
plot(t(1:(end-1)),u(1,:),'--','LineWidth',1.5)
hold on
plot(t(1:(end-1)),u(2,:),'--','LineWidth',1.5)
plot(t(1:(end-1)),u(3,:),'--','LineWidth',1.5)

hold on 
pp1 = plot(t,uc(1,:),'Color',[0 0.4470 0.7410],'LineWidth',0.6);
pp2 = plot(t,uc(2,:),'Color',[0.8500 0.3250 0.0980],'LineWidth',0.6);
pp3 = plot(t,uc(3,:),'Color',[0.9290 0.6940 0.1250],'LineWidth',0.6);
axis([-inf tf -inf inf])


leg = legend([pp1 pp2 pp3],'$\bar{u}_{1}$','$\bar{u}_{2}$','$\bar{u}_{3}$');
leg.Interpreter = 'latex';
grid on 
ylabel('$\bar{\mbox{\textit{\textbf{u}}}}(t)$','Interpreter','latex')
nexttile
plot(t(2:end),-cell2mat(lam),'--','LineWidth',1.5,'Color',[0.6350 0.0780 0.1840])
hold on
plot(t,lamc,'Color',[0.6350 0.0780 0.1840],'LineWidth',0.6)

xlabel('Time [Week]','Interpreter','latex')
ylabel('$\lambda(t)$','Interpreter','latex')
grid on
axis([-inf tf -inf inf])